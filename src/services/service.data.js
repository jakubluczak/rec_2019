class Data {
	constructor() {
		this.apiUrl = '/api/data.json';
	}

	async getData() {
		try {
			const data = await fetch(this.apiUrl);

			if (data.ok) {
				const json = await data.json();
				return json;
			} else {
				throw new Error(data.statusText);
			}
 		} catch (err) {
			throw new Error(err);
		}
	}
}

export default Data;
