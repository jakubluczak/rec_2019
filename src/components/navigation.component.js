import './navigation.component.scss'
export default class NavigationComponent {
	constructor(data) {
		this.data = data;

		// DOM Elements
		this.body = document.querySelector('body');
		this.navigationElement = document.querySelector('.leftNav');
		this.yearsContainer = this.navigationElement.querySelector('.leftNav__elements--year');
		this.brandsContainer = this.navigationElement.querySelector('.leftNav__elements--brand');
		this.modelsContainer = this.navigationElement.querySelector('.leftNav__elements--model');
		this.navHeaders = this.navigationElement.querySelectorAll('.leftNav__header');
		this.modelHeader = this.navigationElement.querySelector('.header--model');
		this.brandHeader = this.navigationElement.querySelector('.header--brand');
		this.yearsHeader = this.navigationElement.querySelector('.header--years');
		this.growsLosesButtons = this.navigationElement.querySelectorAll('.header--button');

		this.activeClassName = 'is-active';
		this.selected = {
			brand: null,
			model: null,
			year: null,
			analysisType: 'gainReal'
		};
	}

	get _itemContainer() {
		const ul = document.createElement('ul');
		ul.classList.add('leftNav__list');

		return ul;
	}

	bindNavigationItems() {
		this._bindGrowsLosesButtons();
		this._bindNavigationButtons();
		this._bindYearsDisplay();
		this._bindBrandsDisplay();
		this._bindModelsDisplay();
	}

	_bindNavigationButtons() {
		this.navHeaders.forEach(navHeader => {
			navHeader.addEventListener('click', event => {
				const element = navHeader.dataset.bindElement;
				const menuContainer = this.navigationElement.querySelector(`.${element}`);
				menuContainer.classList.toggle(this.activeClassName);
			});
		});
	}

	_bindGrowsLosesButtons() {
		this.growsLosesButtons.forEach(el => {
			el.addEventListener('click', ({currentTarget}) => {
				this.selected.analysisType = currentTarget.dataset.analysisType;

				this._dispatchEvent();
			})
		})
	}

	_bindYearsDisplay() {
		const yearsMenu = this._getMenuList(this.data.periodList, 'year');
		this.yearsContainer.append(yearsMenu);
		this.yearsHeader.classList.add(this.activeClassName)
	}

	_bindBrandsDisplay() {
		this.body.addEventListener('menuItemClicked', ({detail}) => {
			if (detail.yearChanged) {
				while(this.brandsContainer.firstChild)  {
					this.brandsContainer.removeChild(this.brandsContainer.firstChild);
				}

				const data = this.data && this.data.mainData && this.data.mainData[detail.selected.year];
				if (data && data.length) {
					const brands = data.filter(el => el.isModel === false);
					if (brands.length) {
						const brandNames = brands.map(el => el.name);
						const brandsMenu = this._getMenuList(brandNames, 'brand');
						this.brandsContainer.append(brandsMenu);
						this.brandHeader.classList.add(this.activeClassName)
					} else {
						this.brandHeader.classList.remove(this.activeClassName);
					}
				}
			}
		});
	}

	_bindModelsDisplay() {
		this.body.addEventListener('menuItemClicked', ({detail}) => {
			if (detail.yearChanged) {

				while(this.modelsContainer.firstChild)  {
					this.modelsContainer.removeChild(this.modelsContainer.firstChild);
				}

				const data = this.data && this.data.mainData && this.data.mainData[detail.selected.year];
				if (data && data.length) {
					const models = data.filter(el => el.isModel === true);
					if (models.length) {
						const modelNames = models.map(el => el.name);
						const modelsMenu = this._getMenuList(modelNames, 'model');
						this.modelsContainer.append(modelsMenu);
						this.modelHeader.classList.add(this.activeClassName)
					} else {
						this.modelHeader.classList.remove(this.activeClassName);
					}
				}
			}
		})
	}

	_getMenuList(menuTextArray, type) {
		return menuTextArray.reduce((acc, current) => {
			const li = this._getItemElement(current, type);

			acc.append(li);

			return acc
		}, this._itemContainer);
	}

	_getItemElement(data, type) {
		const li = document.createElement('li');
		li.classList.add('leftNav__menuItem');
		li.innerText = data;
		li.dataset.bindValue = data;
		li.dataset.menuType = type;

		li.addEventListener('click', event => {
			this._bindMenuItemClick(event);
		});
		return li;
	}

	_bindMenuItemClick(event) {
		const target = event.currentTarget;

		const currentlySelectedItem = document.querySelector(`*[data-bind-value="${this.selected[target.dataset.menuType]}"]`);

		if (currentlySelectedItem) {
			currentlySelectedItem.classList.remove('selected');
		}

		this._resetModelAndBrand();
		this.selected[target.dataset.menuType] = target.dataset.bindValue;

		if (target.dataset.menuType === 'year') {
			this._dispatchEvent(true);
		} else {
			this._dispatchEvent();
		}

		this._bindSelectedClasses();
	}

	_dispatchEvent(yearChanged) {
		const customEvent = new CustomEvent('menuItemClicked', {
			detail: {
				yearChanged,
				selected: this.selected,
			}
		});

		this.body.dispatchEvent(customEvent);
	}


	_resetModelAndBrand() {
		this.selected.model = null;
		this.selected.brand = null;
	}

	_bindSelectedClasses() {
		document.querySelectorAll('.selected').forEach(el => el.classList.remove('selected'));
		Object.values(this.selected).forEach(element => {
			const selectedElement = document.querySelector(`*[data-bind-value="${element}"]`);
			if (selectedElement) {
				selectedElement.classList.add('selected');
			}

		})
	};
}

