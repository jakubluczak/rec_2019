import './table.component.scss';

export default class Table {
	constructor(data) {
		this.body = document.querySelector('body');
		this.table = document.querySelector('.table');
		this.data = data;
	}

	bindTable() {
		this.body.addEventListener('menuItemClicked', ({detail}) => {
			this._clearTable();
			const {selected} = detail;
			const {analysisType} = selected;
			// year only
			if (
				selected.year &&
				!selected.brand &&
				!selected.model
			) {
				const allYearsData = this.data.totals;
				const yearData = allYearsData.find(el => el.periodLabel === selected.year);
				this._createTable(yearData, allYearsData, analysisType);
			} else {

				const yearData = this.data.mainData[selected.year];

				// brand
				if (selected.brand && !selected.model) {
					const allBrandsData = yearData.filter(obj => obj.isModel === false);
					const brandData = allBrandsData.find(obj => obj.name === selected.brand);
					this._createTable(brandData, allBrandsData, analysisType);
				}

				// model
				if (!selected.brand && selected.model) {
					const allModelsData = yearData.filter(obj => obj.isModel === true);
					const modelData = allModelsData.find(obj => obj.name === selected.model);
					this._createTable(modelData, allModelsData, analysisType);
				}
			}
		})
	}

	_createTable(selectedElement, allElementsArray, analysisType) {
		const tableContainer = document.createElement('table');
		const header = this._getHeaderHtml(selectedElement, analysisType);

		tableContainer.classList.add('table-container');
		tableContainer.insertAdjacentHTML( 'afterbegin', header );

		const tBody = document.createElement('tbody');

		const tableBody = allElementsArray.reduce((acc, element) => {
			const name = element.name || element.periodLabel;
			const calculations = this._calculate(selectedElement[analysisType], element[analysisType]);
			const html = (`
				<tr>
					<td>${name}</td>
					<td>${Math.abs(element[analysisType])}</td>
					<td>${calculations}</td>
				</tr>
			`);

			acc.insertAdjacentHTML('beforeEnd', html);
			return acc;
		}, tBody);

		tableContainer.append(tableBody);

		this.table.append(tableContainer);
	}

	_calculate(selectedElementValue, otherElementValue) {
		return otherElementValue- selectedElementValue;
	}

	_getHeaderHtml(selectedElement, analysisType) {
		const type = analysisType === 'gainReal' ? 'gains': 'loses';
		const name = selectedElement.name ? selectedElement.name : `${selectedElement.periodLabel} year`;

		return (`
			<caption>Total ${type} in comparision to ${name}</caption>
			<caption>
				${type.charAt(0).toUpperCase() + type.substring(1)} of ${name}: 
				<span class="table__result">${Math.abs(selectedElement[analysisType])}</span>
			</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>${type.charAt(0).toUpperCase() + type.substring(1)}</th>
					<th>Comparision to ${name}</th>
				</tr>
			</thead>
		`)
	}

	_clearTable() {
		while(this.table.firstChild) {
			this.table.removeChild(this.table.firstChild);
		}
	}
}
