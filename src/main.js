import './main.scss';
import './normalize.scss';

import DataService from './services/service.data';
import NavigationComponent from './components/navigation.component';
import Table from './components/table.component';

const dataService = new DataService();

dataService.getData()
	.then(data => {
		const table = new Table(data);
		const navigation = new NavigationComponent(data);
		navigation.bindNavigationItems();
		table.bindTable();
	});
