const path = require('path');
const fs = require('fs');
const express = require('express');
const app = express();


app.use('/', express.static(path.resolve(__dirname, '../static')))

app.get('/api/data.json', (req, res) => {
	fs.readFile(`${__dirname}/data.json`, {encoding: 'utf-8'}, (err, data) => {
		if (err) {
			res.status(500).send(err);
		}

		res.send(data);
	});
});

app.listen('3000', () => console.log('app listens on port 3000'))
